// Copyright (c) 2023 Boomi, Inc.
package boomi.connector.oracledatabase.util;

import com.boomi.connector.oracledatabase.util.QueryBuilderUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.BLOB;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.BOOLEAN;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.DATE;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.DOUBLE;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.FLOAT;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.INTEGER;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.LONG;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.NVARCHAR;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.STRING;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.TIME;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.TIMESTAMP;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class QueryBuilderUtilTest {

    private static final String INPUT =
            "{\r\n" + "\"id\":\"123\",\r\n" + " \"double\":\"7.8368498\",\r\n" + "\"salary\":\"123.00\",\r\n"
                    + " \"Long\":\"1234\",\r\n" + "\"date\":\"2023-04-06\",\r\n" + " \"name\":\"abc\",\r\n"
                    + "\"dob\":\"2019-12-09\",\r\n" + "\"blob\":\"Hello\",\r\n" + "\"isQualified\":true,\r\n"
                    + " \"lapTime\":\"05:33:30\"\r\n" + "\r\n" + " }";
    private static final String INPUT_JSON_NULL_VALUE =
            "{\"id\":30,\"sno\":null,\"name\":null,\"startDate\":null,\"startTime\":null,\"distance\":null,"
                    + "\"salary\":null," + "\"bonus\":null,\"blob\":null,\"isQualified\":null,\"result\":\"Success\"}";
    private static final String START_TIME = "startTime";
    private static final String TIME_STAMP_REF = "05:33:30";
    private static final String SAMPLE_DATE = "2023-04-06";
    private static final float SAMPLE_FLOAT = 123.00f;
    private static final double SAMPLE_DOUBLE = 123.0d;
    private static final long SAMPLE_LONG = 1234L;
    private final PreparedStatement _preparedStatement = mock(PreparedStatement.class);
    private final Map<String, String> _dataTypes = new HashMap<>();
    private final BigDecimal _bigDecimal = new BigDecimal(123);
    private static final int count = 1;
    private final ObjectMapper _objectMapper = new ObjectMapper();

    @Test
    public void testCheckDataTypeIsIntegerWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("sno");
        _dataTypes.put("integer", INTEGER);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, INTEGER, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.INTEGER);
    }

    @Test
    public void testCheckDataTypeIsInteger() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("id");
        _dataTypes.put("integer", INTEGER);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, INTEGER, fieldName, count);
        verify(_preparedStatement, times(1)).setBigDecimal(1, _bigDecimal);
    }

    @Test
    public void testCheckDataTypeIsDateWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("startDate");
        _dataTypes.put("date", DATE);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, DATE, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.DATE);
    }

    @Test
    public void testCheckDataTypeIsDate() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("date");
        _dataTypes.put("date", DATE);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, DATE, fieldName, count);
        verify(_preparedStatement, times(1)).setString(1, SAMPLE_DATE);
    }

    @Test
    public void testCheckDataTypeIsStringWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("name");
        _dataTypes.put("string", STRING);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, STRING, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.VARCHAR);
    }

    @Test
    public void testCheckDataTypeIsString() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("name");
        _dataTypes.put("string", STRING);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, STRING, fieldName, count);
        verify(_preparedStatement, times(1)).setString(1, "abc");
    }

    @Test
    public void testCheckDataTypeIsNvarcharWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("name");
        _dataTypes.put("nvarchar", NVARCHAR);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, NVARCHAR, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.NVARCHAR);
    }

    @Test
    public void testCheckDataTypeIsNvarchar() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("name");
        _dataTypes.put("nvarchar", NVARCHAR);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, NVARCHAR, fieldName, count);
        verify(_preparedStatement, times(1)).setString(1, "abc");
    }

    @Test
    public void testCheckDataTypeIsTimeWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get(START_TIME);
        _dataTypes.put("time", TIME);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, TIME, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.TIME);
    }

    @Test
    public void testCheckDataTypeIsTime() throws SQLException, IOException {
        Time _sqlTime = new Time(05, 33, 30);
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("lapTime");
        _dataTypes.put("time", TIME);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, TIME, fieldName, count);
        verify(_preparedStatement, times(1)).setTime(1, Time.valueOf(_sqlTime.toString()));
    }

    @Test
    public void testCheckDataTypeIsBooleanWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("isQualified");
        _dataTypes.put("boolean", BOOLEAN);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, BOOLEAN, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.BOOLEAN);
    }

    @Test
    public void testCheckDataTypeIsBoolean() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("isQualified");
        _dataTypes.put("boolean", BOOLEAN);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, BOOLEAN, fieldName, count);
        verify(_preparedStatement, times(1)).setBoolean(1, true);
    }

    @Test
    public void testCheckDataTypeIsLongWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("distance");
        _dataTypes.put("long", LONG);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, LONG, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.BIGINT);
    }

    @Test
    public void testCheckDataTypeIsLong() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("Long");
        _dataTypes.put("long", LONG);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, LONG, fieldName, count);
        verify(_preparedStatement, times(1)).setLong(1, SAMPLE_LONG);
    }

    @Test
    public void testCheckDataTypeIsFloatWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("salary");
        _dataTypes.put("float", FLOAT);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, FLOAT, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.FLOAT);
    }

    @Test
    public void testCheckDataTypeIsFloat() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("salary");
        _dataTypes.put("float", FLOAT);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, FLOAT, fieldName, count);
        verify(_preparedStatement, times(1)).setFloat(1, SAMPLE_FLOAT);
    }

    @Test
    public void testCheckDataTypeIsDoubleWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("bonus");
        _dataTypes.put("double", DOUBLE);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, DOUBLE, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.DECIMAL);
    }

    @Test
    public void testCheckDataTypeIsDouble() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("salary");
        _dataTypes.put("double", DOUBLE);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, DOUBLE, fieldName, count);
        verify(_preparedStatement, times(1)).setDouble(1, SAMPLE_DOUBLE);
    }

    @Test
    public void testCheckDataTypeIsBlobWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get("blob");
        _dataTypes.put("BLOB", BLOB);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, BLOB, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.BLOB);
    }

    @Test
    public void testCheckDataTypeBlob() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("blob");
        _dataTypes.put("BLOB", BLOB);
        ArgumentCaptor<InputStream> inputStreamCaptor = ArgumentCaptor.forClass(InputStream.class);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, BLOB, fieldName, count);
        verify(_preparedStatement, times(1)).setBlob(eq(1), inputStreamCaptor.capture());
    }

    @Test
    public void testCheckDataTypeIsTimestampWithNull() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT_JSON_NULL_VALUE);
        JsonNode fieldName = jsonNode.get(START_TIME);
        _dataTypes.put("timestamp", TIMESTAMP);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, TIMESTAMP, fieldName, count);
        verify(_preparedStatement, times(1)).setNull(1, Types.TIMESTAMP);
    }

    @Test
    public void testCheckDataTypeIsTimestamp() throws SQLException, IOException {
        JsonNode jsonNode = _objectMapper.readTree(INPUT);
        JsonNode fieldName = jsonNode.get("lapTime");
        _dataTypes.put("timestamp", TIMESTAMP);

        QueryBuilderUtil.checkDataType(_preparedStatement, _dataTypes, TIMESTAMP, fieldName, count);
        verify(_preparedStatement, times(1)).setString(1, TIME_STAMP_REF);
    }
}
